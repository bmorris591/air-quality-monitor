package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class LocalCredentialsDeserialiserTest {
    private val mapper = JsonMapper.builder().findAndAddModules().build()

    @Test
    fun `Credentials encrypted in json are automatically decrypted`() {
        val credentials = LocalCredentials("BBB", "passwordpassword")
        val encrypted = encode(credentials, mapper)
        val json = "{\"credentials\":\"$encrypted\"}"

        val wrapper = mapper.readValue<CredentialsWrapper>(json)

        assertThat(wrapper).isNotNull()
        assertThat(wrapper.credentials).isEqualTo(credentials)
    }
}

@JvmRecord
data class CredentialsWrapper(val credentials: DeviceCredentials)
