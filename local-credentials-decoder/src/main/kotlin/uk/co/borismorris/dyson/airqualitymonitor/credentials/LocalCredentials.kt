package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.annotation.JsonProperty
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials

@JvmRecord
data class LocalCredentials(
    @JsonProperty("serial")
    override val serial: String,
    @JsonProperty("apPasswordHash")
    override val password: String,
) : DeviceCredentials {
    override fun toString() = "DeviceCredentials(serial='$serial', password='XXX')"
}
