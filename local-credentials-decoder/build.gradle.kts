plugins {
    `java-library`
}

dependencies {
    api(projects.deviceApi)

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}
