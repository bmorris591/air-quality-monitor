package uk.co.borismorris.dyson.airqualitymonitor.locator.dns

import io.micrometer.observation.annotation.Observed
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import java.net.InetSocketAddress

@Observed
class DnsDysonDeviceLocator(private val config: DnsLocatorConfig) : DysonDeviceLocator {

    override fun locateDevice(device: DeviceMetaData) = DnsLocatedDevice(device, device.unresolvedAddress())

    private fun DeviceMetaData.unresolvedAddress() = InetSocketAddress.createUnresolved(hostName(), config.mqttPort)

    private fun DeviceMetaData.hostName() =
        listOf(serial, config.searchDomain)
            .filter { it.isNotBlank() }
            .joinToString(separator = ".")
}

data class DnsLocatedDevice(val device: DeviceMetaData, override val mqttAddress: InetSocketAddress) : LocatedDevice, DeviceMetaData by device
