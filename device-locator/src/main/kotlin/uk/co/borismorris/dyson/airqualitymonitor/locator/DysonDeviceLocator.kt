package uk.co.borismorris.dyson.airqualitymonitor.locator

import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData
import java.net.InetSocketAddress

interface DysonDeviceLocator {
    fun locateDevice(device: DeviceMetaData): LocatedDevice
}

interface LocatedDevice : DeviceMetaData {
    val mqttAddress: InetSocketAddress
}
