package uk.co.borismorris.dyson.airqualitymonitor.file

import java.io.InputStream

interface FileBasedDiscoveryConfig {
    val deviceSource: () -> InputStream
}
