package uk.co.borismorris.dyson.airqualitymonitor.file

import com.fasterxml.jackson.annotation.JsonProperty
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData

@JvmRecord
data class StaticMetaData(
    @JsonProperty("Serial")
    override val serial: String,
    @JsonProperty("Name")
    override val name: String,
    @JsonProperty("LocalCredentials")
    override val credentials: DeviceCredentials,
    @JsonProperty("ProductType")
    override val type: Int,
    @JsonProperty("ConnectionType")
    override val connectionType: String,
) : DeviceMetaData

@JvmRecord
data class StaticDeviceCredentials(
    override val serial: String,
    override val password: String,
) : DeviceCredentials
