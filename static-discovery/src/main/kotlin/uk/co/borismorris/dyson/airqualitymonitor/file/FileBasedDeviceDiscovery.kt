package uk.co.borismorris.dyson.airqualitymonitor.file

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.annotation.Observed
import uk.co.borismorris.dyson.airqualitymonitor.discovery.DysonDeviceDiscovery

@Observed
class FileBasedDeviceDiscovery(private val config: FileBasedDiscoveryConfig, mapper: ObjectMapper) :
    DysonDeviceDiscovery {
    private val reader = mapper.readerForListOf(StaticMetaData::class.java)

    override fun discoverDysonDevices() = config.deviceSource().use {
        reader.readValue<List<StaticMetaData>>(it).asSequence()
    }
}
