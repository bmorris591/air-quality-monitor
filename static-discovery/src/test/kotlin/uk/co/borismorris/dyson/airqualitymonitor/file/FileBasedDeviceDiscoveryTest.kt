package uk.co.borismorris.dyson.airqualitymonitor.file

import com.fasterxml.jackson.databind.json.JsonMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import java.io.InputStream

@TestInstance(PER_CLASS)
internal class FileBasedDeviceDiscoveryTest {
    val mapper = JsonMapper.builder().findAndAddModules().build()

    @Test
    fun `I can read devices from a file`() {
        val file = FileBasedDeviceDiscoveryTest::class.java.getResource("./device-file.json")
        val config =
            object : FileBasedDiscoveryConfig {
                override val deviceSource: () -> InputStream
                    get() = { file.openStream() }
            }

        val discovery = FileBasedDeviceDiscovery(config, mapper)
        val devices = discovery.discoverDysonDevices().toList()

        assertThat(devices).hasSize(2)
    }
}
