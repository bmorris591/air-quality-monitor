plugins {
    `java-library`
}

dependencies {
    api(projects.deviceDiscovery)

    implementation(projects.localCredentialsDecoder)
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}
