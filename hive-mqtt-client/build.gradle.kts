plugins {
    `java-library`
}

dependencies {
    api(projects.dysonMqttClient)

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.hivemq.mqtt.client)
}
