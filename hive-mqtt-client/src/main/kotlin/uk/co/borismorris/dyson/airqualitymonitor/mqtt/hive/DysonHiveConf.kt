package uk.co.borismorris.dyson.airqualitymonitor.mqtt.hive

import java.time.Duration
import kotlin.math.ceil
import kotlin.math.roundToLong

interface DysonHiveConf {
    val pollingInterval: Duration
    val pollingGraceMultiplier: Double
    val retries: Int
}

fun DysonHiveConf.receiveTimeout(): Duration = Duration.ofMillis(ceil(pollingInterval.toMillis() * pollingGraceMultiplier).roundToLong())
