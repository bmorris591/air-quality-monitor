package uk.co.borismorris.dyson.airqualitymonitor.mqtt.hive

import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream
import com.fasterxml.jackson.module.kotlin.readValue
import com.hivemq.client.internal.mqtt.lifecycle.MqttClientAutoReconnectImpl
import com.hivemq.client.mqtt.MqttClient
import com.hivemq.client.mqtt.MqttClientExecutorConfig
import com.hivemq.client.mqtt.MqttGlobalPublishFilter
import com.hivemq.client.mqtt.datatypes.MqttQos
import com.hivemq.client.mqtt.lifecycle.MqttClientAutoReconnect
import com.hivemq.client.mqtt.lifecycle.MqttClientDisconnectedContext
import com.hivemq.client.mqtt.lifecycle.MqttClientDisconnectedListener
import com.hivemq.client.mqtt.mqtt3.message.auth.Mqtt3SimpleAuth
import com.hivemq.client.mqtt.mqtt3.message.connect.connack.Mqtt3ConnAck
import io.github.oshai.kotlinlogging.KotlinLogging
import io.micrometer.observation.Observation
import io.micrometer.observation.Observation.Event
import io.micrometer.observation.ObservationRegistry
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.BlockingDysonMqttSubscription
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConnection
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttNotConnectedError
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType.REQUEST_ENVIRONMENT_DATA
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.commandTopic
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.registerDeviceStateModule
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.statusTopic
import java.nio.charset.StandardCharsets
import java.time.ZonedDateTime
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class HivemqDysonMqttClient(
    private val observationRegistry: ObservationRegistry,
    private val executor: Executor,
    private val objectMapper: ObjectMapper,
    private val dysonConfig: DysonHiveConf,
) :
    DysonMqttClient {

    override fun connect(device: LocatedDevice): DysonMqttConnection =
        Observation.createNotStarted("connect", observationRegistry)
            .lowCardinalityKeyValue("device.serial", device.serial)
            .lowCardinalityKeyValue("device.name", device.name)
            .lowCardinalityKeyValue("device.type", device.type.toString())
            .lowCardinalityKeyValue("device.mqtt", device.mqttAddress.toString())
            .observe(
                Supplier {
                    val connection =
                        HivemqDysonMqttConnection(observationRegistry, dysonConfig, device, executor, objectMapper)
                    val connAck = connection.connect()
                    logger.info { "MqttConnectResult(serial='${device.serial}', returnCode='${connAck.returnCode}')" }
                    connection
                },
            ) ?: error("Failed to connect to $device")

    override fun close() {}
}

class HivemqDysonMqttConnection(
    private val observationRegistry: ObservationRegistry,
    private val dysonConfig: DysonHiveConf,
    override val device: LocatedDevice,
    executor: Executor,
    objectMapper: ObjectMapper,
) : DysonMqttConnection {
    private val objectMapper =
        objectMapper.copy()
            .setInjectableValues(InjectableValues.Std().addValue(LocatedDevice::class.java, device))
            .registerDeviceStateModule(device)
    private val statusTopic = device.statusTopic()
    private val automaticReconnectHandler = AutomaticReconnectHandler()
    private val mqttClient =
        MqttClient.builder()
            .identifier("air-quality-monitor")
            .serverAddress(device.mqttAddress)
            .executorConfig(MqttClientExecutorConfig.builder().nettyExecutor(executor).build())
            .useMqttVersion3()
            .simpleAuth(device.simpleAuth())
            .addConnectedListener { logger.info { "Connected $device. $it" } }
            .addDisconnectedListener(automaticReconnectHandler)
            .buildBlocking()
    private val publishes = mqttClient.publishes(MqttGlobalPublishFilter.ALL)

    private val connected = AtomicBoolean(false)

    private fun LocatedDevice.simpleAuth() =
        Mqtt3SimpleAuth.builder()
            .username(credentials.serial)
            .password(credentials.password.toByteArray(StandardCharsets.UTF_8))
            .build()

    private fun MqttRequestMessage.publish() {
        mqttClient.publishWith()
            .topic(device.commandTopic())
            .qos(MqttQos.AT_MOST_ONCE)
            .payload(toBytes())
            .send()
    }

    private fun MqttRequestMessage.toBytes() = objectMapper.writeValueAsBytes(this)

    fun connect(): Mqtt3ConnAck = Observation.createNotStarted("connect", observationRegistry)
        .observe(
            Supplier {
                val connAck = mqttClient.connect()
                logger.info { "Connected to $device - $connAck" }
                connected.set(true)
                connAck
            },
        ) ?: error("Failed to connect to $device")

    override fun monitorDevice(): BlockingDysonMqttSubscription {
        if (!connected.get()) {
            throw DysonMqttNotConnectedError(device)
        }
        val subAck = mqttClient.subscribeWith().topicFilter(statusTopic).send()
        logger.info { "Connected to MQTT state topic $statusTopic for $device. $subAck" }

        return object : BlockingDysonMqttSubscription {
            override fun hasNext() = connected.get()

            override fun next(): MqttResponseMessage {
                val observation = Observation.createNotStarted("next-message", observationRegistry)
                    .lowCardinalityKeyValue("device.serial", device.serial)
                    .lowCardinalityKeyValue("device.name", device.name)
                    .lowCardinalityKeyValue("device.type", device.type.toString())
                    .lowCardinalityKeyValue("device.mqtt", device.mqttAddress.toString())
                return observation.observe(
                    Supplier {
                        if (!connected.get()) {
                            throw DysonMqttNotConnectedError(device)
                        }
                        val request = MqttRequestMessage(REQUEST_ENVIRONMENT_DATA, ZonedDateTime.now())
                        logger.info { "Sending environment status request $request" }
                        request.publish()
                        observation.event(Event.of("Sent environment status request"))
                        val response = publishes.receive(dysonConfig.receiveTimeout().toMillis(), MILLISECONDS)
                            .orElseThrow { TimeoutException("Failed to receive message for $device within the timeout") }
                        logger.info { "Got message $response, for $device" }
                        observation.event(Event.of("Got environment status response"))
                        val payload =
                            response.payload.orElseThrow { NullPointerException("Message $response has no payload") }
                        val responseMessage =
                            ByteBufferBackedInputStream(payload).use { objectMapper.readValue<MqttResponseMessage>(it) }
                        logger.info { "Decoded message. $responseMessage" }
                        responseMessage
                    },
                ) ?: error("Failed to get next message for $device")
            }

            override fun close() {
                logger.info { "State stream $device closed. Send unsubscribe." }
                mqttClient.unsubscribeWith().topicFilter(statusTopic).send()
            }
        }
    }

    override fun close() {
        Observation.createNotStarted("close", observationRegistry)
            .lowCardinalityKeyValue("device.serial", device.serial)
            .lowCardinalityKeyValue("device.name", device.name)
            .lowCardinalityKeyValue("device.type", device.type.toString())
            .lowCardinalityKeyValue("device.mqtt", device.mqttAddress.toString())
            .observe {
                connected.set(false)
                logger.info { "Dispose MQTT connection $mqttClient for device $device" }
                publishes.close()
                mqttClient.disconnect()
                logger.info { "MQTT connection disposed for device $device" }
            }
    }

    inner class AutomaticReconnectHandler(private val autoReconnect: MqttClientAutoReconnect = MqttClientAutoReconnectImpl.DEFAULT) :
        MqttClientDisconnectedListener {
        override fun onDisconnected(context: MqttClientDisconnectedContext) {
            val reconnector = context.reconnector
            if (reconnector.attempts <= dysonConfig.retries) {
                autoReconnect.onDisconnected(context)
            } else {
                logger.warn { "Out of retries to $device. $context" }
            }
        }
    }
}
