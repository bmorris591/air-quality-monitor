plugins {
    application
}

apply(plugin = "org.springframework.boot")
apply(plugin = "org.graalvm.buildtools.native")

val mainClassKt = "uk.co.borismorris.dyson.airqualitymonitor.mock.MockDysonFanKt"

dependencies {
    implementation(projects.dysonMqttClient)

    implementation("org.springframework:spring-web")
    implementation("org.yaml:snakeyaml")

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("io.micrometer:micrometer-registry-otlp")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")

    implementation(libs.hivemq.mqtt.client)

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

application {
    mainClass.set(mainClassKt)
}
