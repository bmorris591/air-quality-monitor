package uk.co.borismorris.dyson.airqualitymonitor.mock.worker

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream
import com.fasterxml.jackson.module.kotlin.readValue
import com.hivemq.client.mqtt.MqttClient
import com.hivemq.client.mqtt.MqttGlobalPublishFilter
import com.hivemq.client.mqtt.datatypes.MqttQos
import com.hivemq.client.mqtt.mqtt3.Mqtt3BlockingClient
import com.hivemq.client.mqtt.mqtt3.message.auth.Mqtt3SimpleAuth
import com.hivemq.client.mqtt.mqtt3.message.publish.Mqtt3Publish
import io.github.oshai.kotlinlogging.KotlinLogging
import io.micrometer.observation.Observation
import io.micrometer.observation.Observation.Event
import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import uk.co.borismorris.dyson.airqualitymonitor.mock.conf.MockFanConf
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.CurrentHeatCapableDeviceState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.heatCapableDeviceModule
import java.nio.charset.StandardCharsets.UTF_8
import java.time.ZonedDateTime

private val logger = KotlinLogging.logger {}

class DysonFanRunner(
    private val observationRegistry: ObservationRegistry,
    private val mockFanConf: MockFanConf,
    objectMapper: ObjectMapper,
) :
    ApplicationRunner,
    AutoCloseable {
    private val objectMapper =
        objectMapper.copy()
            .registerModule(heatCapableDeviceModule)

    private val mqttClient =
        MqttClient.builder()
            .identifier("mock-fan")
            .automaticReconnectWithDefaultConfig()
            .serverHost(mockFanConf.host)
            .serverPort(mockFanConf.mqttPort)
            .useMqttVersion3()
            .simpleAuth(mockFanConf.simpleAuth())
            .addConnectedListener { logger.info { "Connected $mockFanConf. $it" } }
            .buildBlocking()

    private fun MockFanConf.simpleAuth() =
        Mqtt3SimpleAuth.builder()
            .username(username)
            .password(password.toByteArray(UTF_8))
            .build()

    override fun run(args: ApplicationArguments?) {
        mqttClient.publishes(MqttGlobalPublishFilter.ALL).use { publishes ->
            val conncAck = mqttClient.connect()
            logger.info { "Connected to MQTT command topic. $conncAck" }
            val subAck = mqttClient.subscribeWith().topicFilter(mockFanConf.incoming).send()
            logger.info { "Subscribed to MQTT comment topic ${mockFanConf.incoming}. $subAck" }
            while (true) {
                respond(publishes)
            }
        }
    }

    private fun respond(publishes: Mqtt3BlockingClient.Mqtt3Publishes) {
        val observation = Observation.createNotStarted("respond", observationRegistry)
            .highCardinalityKeyValue("topic.incoming", mockFanConf.incoming)
            .highCardinalityKeyValue("topic.outgoing", mockFanConf.outgoing)
        observation.observe {
            val message = publishes.receive()
            observation.event(Event.of("Message received"))
            val payload = message.payload
            val request =
                ByteBufferBackedInputStream(payload.get()).use { objectMapper.readValue<MqttRequestMessage>(it) }
            logger.debug { "Decoded message. $request" }
            observation.event(Event.of("Message decoded"))
            val response = MqttDeviceStatusResponse(CurrentHeatCapableDeviceState()).apply {
                time = ZonedDateTime.now()
                type = MessageType.DEVICE_CURRENT_STATE
            }
            logger.debug { "Sending $response" }

            val responseMessage = Mqtt3Publish.builder()
                .topic(mockFanConf.outgoing)
                .qos(MqttQos.AT_MOST_ONCE)
                .payload(response.toBytes())
                .build()
            mqttClient.publish(responseMessage)
            observation.event(Event.of("Response sent"))
        }
    }

    private fun Any.toBytes() = objectMapper.writeValueAsBytes(this)

    override fun close() {
        mqttClient.disconnect()
    }
}
