package uk.co.borismorris.dyson.airqualitymonitor.mock

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.core.instrument.Metrics
import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import uk.co.borismorris.dyson.airqualitymonitor.mock.conf.MockFanConf
import uk.co.borismorris.dyson.airqualitymonitor.mock.worker.DysonFanRunner

@EnableAutoConfiguration
@Configuration(proxyBeanMethods = false)
class MockDysonFan {
    @ConfigurationProperties(prefix = "dyson.mockfan")
    @Bean
    fun mockFanConfiguration() = MockFanConf()

    @Bean
    fun dysonFanRunner(
        observationRegistry: ObservationRegistry,
        mockFanConf: MockFanConf,
        objectMapper: ObjectMapper,
    ) = DysonFanRunner(observationRegistry, mockFanConf, objectMapper)

    @Bean
    @ConditionalOnClass(name = ["io.opentelemetry.javaagent.OpenTelemetryAgent"])
    fun otelRegistry() =
        Metrics.globalRegistry.registries.find { r -> r.javaClass.name.contains("OpenTelemetryMeterRegistry") }?.also {
            Metrics.globalRegistry.remove(it)
        }
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    SpringApplication(MockDysonFan::class.java).apply {
        webApplicationType = WebApplicationType.NONE
    }.run(*args)
}
