package uk.co.borismorris.dyson.airqualitymonitor.device

interface DeviceMetaData {
    val serial: String
    val name: String
    val credentials: DeviceCredentials
    val type: Int
    val connectionType: String

    val heatCapable: Boolean
        get() =
            when (type) {
                527 -> true
                else -> false
            }
}
