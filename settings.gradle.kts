rootProject.name = "dyson-influxdb"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        maven("https://repo.spring.io/milestone")
        maven("https://gitlab.com/api/v4/projects/26731120/packages/maven")
    }
}

include("device-api")
include("local-credentials-decoder")
include("device-locator")
include("dns-device-locator")
include("device-discovery")
include("static-discovery")
include("dyson-mqtt-client")
include("hive-mqtt-client")
include("mock-dyson-fan")
include("air-quality-monitor")
