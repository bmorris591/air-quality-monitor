package uk.co.borismorris.dyson.airqualitymonitor

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.findUnmatchedRequests
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import com.hivemq.client.mqtt.MqttClient
import com.hivemq.client.mqtt.MqttGlobalPublishFilter
import com.hivemq.client.mqtt.mqtt3.Mqtt3BlockingClient
import com.hivemq.client.mqtt.mqtt3.Mqtt3BlockingClient.Mqtt3Publishes
import com.hivemq.client.mqtt.mqtt3.message.connect.connack.Mqtt3ConnAckReturnCode
import io.github.oshai.kotlinlogging.KotlinLogging
import io.moquette.BrokerConstants.AUTHENTICATOR_CLASS_NAME
import io.moquette.BrokerConstants.DISABLED_PORT_BIND
import io.moquette.BrokerConstants.ENABLE_TELEMETRY_NAME
import io.moquette.BrokerConstants.HOST_PROPERTY_NAME
import io.moquette.BrokerConstants.PASSWORD_FILE_PROPERTY_NAME
import io.moquette.BrokerConstants.WEB_SOCKET_PORT_PROPERTY_NAME
import io.moquette.broker.Server
import io.moquette.broker.config.MemoryConfig
import io.moquette.broker.security.IAuthenticator
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.CurrentDeviceState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DeviceStateChange
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.FanSpeed
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType.REQUEST_ENVIRONMENT_DATA
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusChangeResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttEnvironmentDataResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MutableEnvironmentData
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.PropertyChange
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.ToggleState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.heatCapableDeviceModule
import java.net.InetAddress
import java.time.Duration
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.TimeUnit.MINUTES

const val MQTT_PORT = 1883
private val logger = KotlinLogging.logger {}

@SpringBootTest(
    classes = [AirQualityMonitorApplication::class],
    properties = [
        "dyson.mqtt-port=$MQTT_PORT",
        "dyson.search-domain",
    ],
)
class AirQualityMonitorApplicationTest {
    companion object {
        @JvmField
        @RegisterExtension
        val wiremock = WireMockExtension.newInstance()
            .options(
                options()
                    .dynamicPort()
                    .usingFilesUnderClasspath("uk/co/borismorris/dyson/airqualitymonitor/wiremock")
                    .notifier(Slf4jNotifier(true)),
            )
            .configureStaticDsl(true)
            .build()

        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("influx.url") { wiremock.baseUrl() }
        }
    }

    lateinit var mqttClient: Mqtt3BlockingClient
    lateinit var mqttPublishes: Mqtt3Publishes

    val mqttBroker = Server()

    @BeforeEach
    fun setup() {
        val config = MemoryConfig(Properties()).apply {
            setProperty(WEB_SOCKET_PORT_PROPERTY_NAME, DISABLED_PORT_BIND)
            setProperty(HOST_PROPERTY_NAME, InetAddress.getLoopbackAddress().canonicalHostName)
            setProperty(PASSWORD_FILE_PROPERTY_NAME, "/moquette/password_file")
            setProperty(AUTHENTICATOR_CLASS_NAME, HardcodedAuthenticator::class.qualifiedName)
            setProperty(ENABLE_TELEMETRY_NAME, false.toString())
        }
        mqttBroker.startServer(config)

        mqttClient = MqttClient.builder()
            .identifier("test-client")
            .serverHost("localhost")
            .serverPort(MQTT_PORT)
            .useMqttVersion3()
            .buildBlocking()

        val connectionResult = mqttClient.connect()
        assertThat(connectionResult.returnCode).`as`("Connection success").isEqualTo(Mqtt3ConnAckReturnCode.SUCCESS)
        mqttPublishes = mqttClient.publishes(MqttGlobalPublishFilter.SUBSCRIBED)
        mqttClient.subscribeWith().topicFilter("527/localhost/command").send()
    }

    @AfterEach
    fun teardown() {
        mqttPublishes.close()
        mqttClient.disconnect()
        mqttBroker.stopServer()
    }

    @AfterEach
    fun noUnmatched() {
        assertThat(findUnmatchedRequests()).isEmpty()
    }

    @Test
    fun `Integration test environment data poll`() {
        waitForPoll()

        val environmentData = MutableEnvironmentData().apply {
            temperature = 700.0
            humidity = 73
        }
        val responseMessage = MqttEnvironmentDataResponse(environmentData).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.ENVIRONMENT_DATA
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    @Test
    fun `Integration test purecool state`() {
        waitForPoll()

        val deviceState = CurrentDeviceState().apply {
            fanStatus = ToggleState.ON
            nightModeSpeed = 1
            fanSpeed = FanSpeed.forValue(10.toString())
        }
        val responseMessage = MqttDeviceStatusResponse(deviceState).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.DEVICE_CURRENT_STATE
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    @Test
    fun `Integration test purecool state change`() {
        waitForPoll()

        val deviceState = DeviceStateChange().apply {
            fanStatusChange = PropertyChange.change(ToggleState.OFF, ToggleState.ON)
            nightModeSpeedChange = PropertyChange.unchanged(3)
            fanSpeedChange = PropertyChange.change(FanSpeed.forValue("AUTO"), FanSpeed.forValue(5.toString()))
        }
        val responseMessage = MqttDeviceStatusChangeResponse(deviceState).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.DEVICE_STATE_CHANGE
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    private fun waitForPoll() {
        val message = mqttPublishes.receive(1, MINUTES).get()
        val request = objectMapper.readValue<MqttRequestMessage>(message.payloadAsBytes)
        with(request) {
            assertThat(type).isEqualTo(REQUEST_ENVIRONMENT_DATA)
            assertThat(time).isBeforeOrEqualTo(ZonedDateTime.now())
        }
    }

    private fun publishMessage(responseMessage: MqttResponseMessage) {
        mqttClient.publishWith().topic("527/localhost/status/current")
            .payload(objectMapper.writeValueAsBytes(responseMessage)).send()
    }
}

class HardcodedAuthenticator : IAuthenticator {
    override fun checkValid(clientId: String, username: String, password: ByteArray): Boolean {
        val passString = String(password)
        logger.info { "clientId:$clientId, username:$username, password:$passString" }
        if (clientId == "test-client") {
            return true
        }
        if (username != "localhost") {
            return false
        }
        if (passString != "mqtt-password") {
            return false
        }
        return true
    }
}

private val objectMapper = Jackson2ObjectMapperBuilder.json()
    .findModulesViaServiceLoader(true)
    .modulesToInstall(heatCapableDeviceModule)
    .build<ObjectMapper>()
