package uk.co.borismorris.dyson.airqualitymonitor.otel

import io.opentelemetry.api.common.Attributes
import io.opentelemetry.sdk.autoconfigure.ResourceConfiguration
import io.opentelemetry.sdk.autoconfigure.spi.ConfigProperties
import io.opentelemetry.sdk.autoconfigure.spi.ResourceProvider
import io.opentelemetry.sdk.autoconfigure.spi.internal.ConditionalResourceProvider
import io.opentelemetry.sdk.resources.Resource
import io.opentelemetry.semconv.ResourceAttributes
import org.springframework.boot.actuate.autoconfigure.opentelemetry.OpenTelemetryAutoConfiguration
import org.springframework.boot.actuate.autoconfigure.opentelemetry.OpenTelemetryProperties
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.env.PropertyResolver
import java.time.Duration
import java.util.*

private const val DEFAULT_APPLICATION_NAME = "application"

@Configuration
@AutoConfigureBefore(OpenTelemetryAutoConfiguration::class)
class OtelResources(private val propertyResolver: PropertyResolver) {
    private val configProperties = SpringEnvironmentConfigProperties(propertyResolver)

    @Bean
    @Primary
    fun openTelemetryResource(properties: OpenTelemetryProperties): Resource {
        val applicationName = propertyResolver.getProperty("spring.application.name", DEFAULT_APPLICATION_NAME)
        return Resource.getDefault()
            .merge(providerResources())
            .merge(envResources())
            .merge(Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, applicationName)))
            .merge(springBootResources(properties))
    }

    private fun providerResources(): Resource = ServiceLoader.load(ResourceProvider::class.java)
        .fold(Resource.getDefault()) { resource, provider ->
            if (provider is ConditionalResourceProvider && !provider.shouldApply(configProperties, resource)) {
                resource
            } else {
                resource.merge(provider.createResource(configProperties))
            }
        }

    private fun envResources(): Resource =
        ResourceConfiguration.createEnvironmentResource(configProperties)

    private fun springBootResources(properties: OpenTelemetryProperties): Resource = Resource.builder()
        .also { properties.resourceAttributes.forEach { (key, value) -> it.put(key, value) } }
        .build()
}

class SpringEnvironmentConfigProperties(private val propertyResolver: PropertyResolver) : ConfigProperties {
    override fun getString(key: String): String? = propertyResolver.getProperty(key, String::class.java)

    override fun getBoolean(key: String): Boolean? = propertyResolver.getProperty(key, Boolean::class.java)
    override fun getInt(key: String): Int? = propertyResolver.getProperty(key, Int::class.java)

    override fun getLong(key: String): Long? = propertyResolver.getProperty(key, Long::class.java)

    override fun getDouble(key: String): Double? = propertyResolver.getProperty(key, Double::class.java)
    override fun getDuration(key: String): Duration? = propertyResolver.getProperty(key, Duration::class.java)

    override fun getList(key: String): List<String> = getSequence(key).toList()

    override fun getMap(key: String): Map<String, String> = getSequence(key)
        .map { it.split("=", limit = 2) }
        .filter { it[0].isNotBlank() }
        .map { items -> items.asSequence().trim().toList() }
        .filter { it.size == 2 }
        .associate { it[0] to it[1] }

    private fun getSequence(key: String) = getString(key)
        ?.splitToSequence(",")
        ?.trim()
        .orEmpty()

    private fun Sequence<String>.trim() = map { it.trim() }.filter { it.isNotBlank() }
}
