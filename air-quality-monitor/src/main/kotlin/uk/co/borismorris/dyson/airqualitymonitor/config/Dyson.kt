package uk.co.borismorris.dyson.airqualitymonitor.config

import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData
import uk.co.borismorris.dyson.airqualitymonitor.locator.dns.DnsLocatorConfig
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.hive.DysonHiveConf
import java.time.Duration

class DysonConfig : DnsLocatorConfig, DysonHiveConf {
    override lateinit var searchDomain: String
    override var mqttPort = 1883
    override lateinit var pollingInterval: Duration
    var backOffInterval: Duration = Duration.ofSeconds(5)
    override var pollingGraceMultiplier: Double = 2.0
    override var retries = 5

    lateinit var devices: List<DeviceMetadataConfig>
}

class DeviceMetadataConfig : DeviceMetaData {
    override lateinit var serial: String
    override lateinit var name: String
    override val credentials = DeviceCredentialsConfig()
    override var type = -1
    override lateinit var connectionType: String

    override fun toString() = "DeviceMetadataConfig(serial='$serial', name='$name', connectionType='$connectionType', credentials=$credentials, type=$type)"
}

class DeviceCredentialsConfig : DeviceCredentials {
    override lateinit var serial: String
    override lateinit var password: String

    override fun toString() = "DeviceCredentialsConfig(serial='$serial', password='XXXX')"
}
