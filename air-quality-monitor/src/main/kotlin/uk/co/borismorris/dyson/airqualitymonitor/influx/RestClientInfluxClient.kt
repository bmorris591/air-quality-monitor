package uk.co.borismorris.dyson.airqualitymonitor.influx

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.json.JsonMapper
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.domain.Dialect.AnnotationsEnum
import com.influxdb.client.domain.Query
import com.influxdb.client.domain.Query.TypeEnum
import com.influxdb.client.internal.AbstractWriteClient.BatchWriteData
import com.influxdb.client.internal.AbstractWriteClient.BatchWriteDataPoint
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.MediaType.ALL
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_PLAIN
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter
import org.springframework.web.client.RestClient
import java.nio.charset.StandardCharsets.UTF_8

private val logger = KotlinLogging.logger {}

class RestClientInfluxClient(
    restClient: RestClient.Builder,
    private val options: InfluxDBClientOptions,
) :
    InfluxClient {

    private val mapper = JsonMapper.builder()
        .findAndAddModules()
        .addMixIn(TypeEnum::class.java, InfluxEnumMixin::class.java)
        .addMixIn(AnnotationsEnum::class.java, InfluxEnumMixin::class.java)
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .build()
    private val restClient = restClient
        .messageConverters { converters ->
            converters.find { it is AbstractJackson2HttpMessageConverter }?.let { converter ->
                (converter as AbstractJackson2HttpMessageConverter).registerObjectMappersForType(Query::class.java) {
                    it[ALL] = mapper
                }
            }
        }
        .baseUrl(options.url)
        .also { builder ->
            options.token?.let {
                builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Token ${String(it)}")
            }
        }
        .build()

    override fun writePoints(points: List<Point>, parameters: WriteParameters) {
        parameters.check(options)
        points
            .groupBy { it.precision }
            .forEach { (precision, grouped) ->
                val groupParameters = parameters.copy(precision, options)
                write(
                    groupParameters,
                    grouped.asSequence().map { BatchWriteDataPoint(it, options) },
                )
            }
    }

    private fun write(parameters: WriteParameters, stream: Sequence<BatchWriteData>) {
        val lineProtocol = stream.map { it.toLineProtocol() }
            .filterNot { it.isNullOrEmpty() }
            .joinToString(separator = "\n")
        if (lineProtocol.isEmpty()) {
            logger.warn { "The writes: $stream doesn't contains any Line Protocol, skipping" }
            return
        }
        val organization = parameters.orgSafe(options)
        val bucket = parameters.bucketSafe(options)
        val precision = parameters.precisionSafe(options)
        val consistency = parameters.consistencySafe(options)
        logger.trace { "Writing time-series data into InfluxDB (org=$organization, bucket=$bucket, precision=$precision)..." }
        val response = restClient.post()
            .uri {
                it.path("api/v2/write")
                    .queryParam("org", organization)
                    .queryParam("bucket", bucket)
                    .queryParam("precision", precision)
                    .queryParam("consistency", consistency)
                    .build()
            }
            .contentType(MediaType(TEXT_PLAIN, UTF_8))
            .accept(APPLICATION_JSON)
            .body(lineProtocol)
            .retrieve()
            .toBodilessEntity()
        logger.info { "Got response $response" }
    }
}

interface InfluxEnumMixin {
    @JsonValue
    fun getValue(): String
}
