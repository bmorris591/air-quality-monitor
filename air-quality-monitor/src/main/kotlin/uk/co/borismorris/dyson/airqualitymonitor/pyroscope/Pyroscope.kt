package uk.co.borismorris.dyson.airqualitymonitor.pyroscope

import io.github.oshai.kotlinlogging.KotlinLogging
import io.pyroscope.javaagent.PyroscopeAgent
import io.pyroscope.javaagent.PyroscopeAgent.Options
import io.pyroscope.javaagent.api.ConfigurationProvider
import io.pyroscope.javaagent.api.Logger
import io.pyroscope.javaagent.config.Config
import jakarta.annotation.PostConstruct
import org.springframework.core.env.Environment
import java.util.*

private val logger = KotlinLogging.logger {}

class Pyroscope(private val config: ConfigurationProvider) {

    @PostConstruct
    @Suppress("SpreadOperator")
    fun init() {
        val config = Config.build(config)
        logger.info { "Starting Pyroscope with config $config" }

        PyroscopeAgent.start(
            Options.Builder(config)
                .setLogger { l, msg, args ->
                    when (l) {
                        Logger.Level.DEBUG -> logger.debug { String.format(msg, *args) }
                        Logger.Level.INFO -> logger.info { String.format(msg, *args) }
                        Logger.Level.WARN -> logger.warn { String.format(msg, *args) }
                        Logger.Level.ERROR -> logger.error { String.format(msg, *args) }
                        null -> logger.info { String.format(msg, *args) }
                    }
                }
                .build(),
        )
    }
}

class SpringConfigProvider(private val config: Environment) : ConfigurationProvider {
    override fun get(key: String): String? {
        config.getProperty(key)?.let { return it }
        val lowerKey = key.lowercase(Locale.ROOT)
        val trimmedKey = lowerKey.substring(10)
        val kebabKey = trimmedKey.replace('_', '-')
        return config.getProperty("pyroscope.$kebabKey")
    }
}
