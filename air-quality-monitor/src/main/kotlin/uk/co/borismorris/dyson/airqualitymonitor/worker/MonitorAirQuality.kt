package uk.co.borismorris.dyson.airqualitymonitor.worker

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.WriteParameters
import io.github.oshai.kotlinlogging.KotlinLogging
import io.github.oshai.kotlinlogging.withLoggingContext
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import io.opentelemetry.context.Context
import jakarta.annotation.PostConstruct
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.dyson.airqualitymonitor.config.DeviceMetadataConfig
import uk.co.borismorris.dyson.airqualitymonitor.config.DysonConfig
import uk.co.borismorris.dyson.airqualitymonitor.influx.EnvironmentObservationMeasurementConverter.convert
import uk.co.borismorris.dyson.airqualitymonitor.influx.InfluxClient
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.BlockingDysonMqttSubscription
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConnection
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.StructuredTaskScope
import java.util.concurrent.StructuredTaskScope.Subtask
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class MonitorAirQuality(
    private val observationRegistry: ObservationRegistry,
    private val config: DysonConfig,
    private val deviceLocator: DysonDeviceLocator,
    private val mqttClient: DysonMqttClient,
    private val influxClient: InfluxClient,
) {
    private val connections = CopyOnWriteArrayList<DysonDeviceConnection>()

    @PostConstruct
    fun monitor() {
        logger.info { "Starting monitoring!" }
        val devices = config.devices
        logger.info { "Devices $devices. Monitoring." }
        devices.map { DysonDeviceConnection(observationRegistry, mqttClient, deviceLocator, it) }
            .toCollection(connections)
    }

    @Scheduled(initialDelay = 0, fixedRateString = "\${dyson.polling-interval}")
    fun pollDevices() {
        val factory = Thread.ofVirtual().factory()
        StructuredTaskScope<MqttResponseMessage>("pollDevices") { r ->
            factory.newThread(
                Context.current().wrap(r),
            )
        }.use { scope ->
            val tasks = connections.map {
                scope.fork {
                    it.pollDevice()
                }
            }.toList()

            scope.join()

            val (succeeded, failed) = tasks.partition { it.state() == Subtask.State.SUCCESS }

            failed.forEach {
                @Suppress("ThrowingExceptionsWithoutMessageOrCause")
                logger.error(it.exception()) { "Task failed" }
            }

            val points = succeeded.asSequence()
                .flatMap { it.get().convert() }
                .onEach { logger.trace { "Got point ${it.toLineProtocol()}" } }
                .toList()
            influxClient.writePoints(points, WriteParameters(WritePrecision.MS, null))
        }
    }

    data class DysonDeviceConnection(
        private val observationRegistry: ObservationRegistry,
        val mqttClient: DysonMqttClient,
        val deviceLocator: DysonDeviceLocator,
        val device: DeviceMetadataConfig,
    ) : AutoCloseable {

        lateinit var connection: DysonMqttConnection
        lateinit var subscription: BlockingDysonMqttSubscription
        val connected: Boolean
            get() = this::subscription.isInitialized

        private fun connect() {
            Observation.createNotStarted("connect", observationRegistry)
                .lowCardinalityKeyValue("device.serial", device.serial)
                .lowCardinalityKeyValue("device.name", device.name)
                .lowCardinalityKeyValue("device.type", device.type.toString())
                .observe {
                    connection = mqttClient.connect(deviceLocator.locateDevice(device))
                    subscription = connection.monitorDevice()
                }
        }

        @Suppress("TooGenericExceptionCaught")
        fun pollDevice(): MqttResponseMessage = Observation.createNotStarted("poll-device", observationRegistry)
            .lowCardinalityKeyValue("device.serial", device.serial)
            .lowCardinalityKeyValue("device.name", device.name)
            .lowCardinalityKeyValue("device.type", device.type.toString())
            .observe(
                Supplier {
                    withLoggingContext(
                        "deviceSerial" to device.serial,
                        "deviceName" to device.name,
                    ) {
                        if (!connected) {
                            logger.info { "Not connected to $device, establishing connection." }
                            connect()
                        }
                        try {
                            subscription.next()
                        } catch (ex: Exception) {
                            logger.info { "An error occurred polling $device, disconnecting." }
                            close()
                            throw ex
                        }
                    }
                },
            ) ?: error("Failed to poll $device")

        override fun close() {
            Observation.createNotStarted("close", observationRegistry)
                .lowCardinalityKeyValue("device.serial", device.serial)
                .lowCardinalityKeyValue("device.name", device.name)
                .lowCardinalityKeyValue("device.type", device.type.toString())
                .observe {
                    if (this::subscription.isInitialized) {
                        subscription.close()
                    }
                    if (this::connection.isInitialized) {
                        connection.close()
                    }
                }
        }
    }
}
