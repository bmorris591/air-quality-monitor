package uk.co.borismorris.dyson.airqualitymonitor

import com.fasterxml.jackson.databind.ObjectMapper
import com.influxdb.client.InfluxDBClientOptions
import io.micrometer.core.instrument.Metrics.globalRegistry
import io.micrometer.observation.ObservationRegistry
import io.pyroscope.javaagent.api.ConfigurationProvider
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.ClientHttpRequestFactories
import org.springframework.boot.web.client.ClientHttpRequestFactorySettings
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.core.task.TaskExecutor
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.client.RestClient
import uk.co.borismorris.dyson.airqualitymonitor.config.DysonConfig
import uk.co.borismorris.dyson.airqualitymonitor.config.InfluxDB2Properties
import uk.co.borismorris.dyson.airqualitymonitor.influx.InfluxClient
import uk.co.borismorris.dyson.airqualitymonitor.influx.RestClientInfluxClient
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.locator.dns.DnsDysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.locator.dns.DnsLocatorConfig
import uk.co.borismorris.dyson.airqualitymonitor.logging.LogGitInfoOnStartup
import uk.co.borismorris.dyson.airqualitymonitor.logging.LogVersionOnStartup
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.hive.DysonHiveConf
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.hive.HivemqDysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.pyroscope.Pyroscope
import uk.co.borismorris.dyson.airqualitymonitor.pyroscope.SpringConfigProvider
import uk.co.borismorris.dyson.airqualitymonitor.worker.MonitorAirQuality
import java.time.Duration

@EnableConfigurationProperties(InfluxDB2Properties::class)
@SpringBootApplication
class AirQualityMonitorApplication {
    @Bean
    fun influxConfig(properties: InfluxDB2Properties) = InfluxDBClientOptions.builder()
        .apply {
            properties.url?.let { url(it) }
            properties.token?.let { authenticateToken(it.toCharArray()) }
        }
        .bucket(properties.bucket)
        .org(properties.org)
        .build()

    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactories.get(
                SimpleClientHttpRequestFactory::class.java,
                ClientHttpRequestFactorySettings.DEFAULTS
                    .withConnectTimeout(Duration.ofMinutes(5))
                    .withReadTimeout(Duration.ofMinutes(5)),
            ),
        )
    }

    @Bean
    fun influxClient(influxConfig: InfluxDBClientOptions, restClientBuilder: RestClient.Builder) =
        RestClientInfluxClient(restClientBuilder, influxConfig)

    @ConfigurationProperties(prefix = "dyson")
    @Bean
    fun dynsonConfig() = DysonConfig()

    @Bean
    fun dysonDeviceLocator(dnsLocatorConfig: DnsLocatorConfig) = DnsDysonDeviceLocator(dnsLocatorConfig)

    @Bean
    fun dysonMqttClient(
        observationRegistry: ObservationRegistry,
        taskExecutor: TaskExecutor,
        objectMapper: ObjectMapper,
        dysonHiveConf: DysonHiveConf,
    ) =
        HivemqDysonMqttClient(observationRegistry, taskExecutor, objectMapper, dysonHiveConf)

    @Bean
    fun monitorAirQuality(
        observationRegistry: ObservationRegistry,
        config: DysonConfig,
        deviceLocator: DysonDeviceLocator,
        mqttClient: DysonMqttClient,
        influxClient: InfluxClient,
    ) = MonitorAirQuality(observationRegistry, config, deviceLocator, mqttClient, influxClient)

    @Bean
    fun logVersionOnStartup(buildProperties: BuildProperties?) = LogVersionOnStartup(buildProperties)

    @Bean
    fun logGitInfoOnStartup(gitProperties: GitProperties?) = LogGitInfoOnStartup(gitProperties)

    @Bean
    @ConditionalOnClass(name = ["io.opentelemetry.javaagent.OpenTelemetryAgent"])
    fun otelRegistry() =
        globalRegistry.registries.find { r -> r.javaClass.name.contains("OpenTelemetryMeterRegistry") }?.also {
            globalRegistry.remove(it)
        }

    @Configuration
    @ConditionalOnProperty(value = ["scheduling.enabled"], havingValue = "true", matchIfMissing = true)
    @EnableScheduling
    class Scheduling

    @Configuration
    @ConditionalOnProperty(prefix = "pyroscope", name = ["enabled"], matchIfMissing = true)
    class PyroscopeConfig {
        @Bean
        fun springConfigProvider(environment: Environment) = SpringConfigProvider(environment)

        @Bean
        fun pyroscope(configurationProvider: ConfigurationProvider) = Pyroscope(configurationProvider)
    }
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<AirQualityMonitorApplication>(*args) {
        webApplicationType = WebApplicationType.NONE
    }
}
