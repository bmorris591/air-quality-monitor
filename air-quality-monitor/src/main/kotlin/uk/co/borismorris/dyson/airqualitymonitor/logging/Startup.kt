package uk.co.borismorris.dyson.airqualitymonitor.logging

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.context.event.EventListener

private val logger = KotlinLogging.logger {}

class LogGitInfoOnStartup(val gitProperties: GitProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        gitProperties?.apply { logger.info { "$branch@$commitId" } }
    }
}

class LogVersionOnStartup(val buildProperties: BuildProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        buildProperties?.apply { logger.info { "$group:$artifact:$version@$time" } }
    }
}
