package uk.co.borismorris.dyson.airqualitymonitor.influx

import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters

interface InfluxClient {
    fun writePoints(points: List<Point>, parameters: WriteParameters)
}
