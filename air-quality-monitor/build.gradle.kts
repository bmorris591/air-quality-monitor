plugins {
    application

    alias(libs.plugins.jib)
}

apply(plugin = "org.springframework.boot")
apply(plugin = "org.graalvm.buildtools.native")

val mainClassKt = "uk.co.borismorris.dyson.airqualitymonitor.AirQualityMonitorApplicationKt"

testing {
    suites {
        val test by getting(JvmTestSuite::class)
        val integrationTest by registering(JvmTestSuite::class) {
            testType.set(TestSuiteType.INTEGRATION_TEST)

            sources {
                compileClasspath += test.sources.output
                runtimeClasspath += test.sources.output
            }

            dependencies {
                implementation(project())
            }

            targets {
                all {
                    testTask.configure {
                        shouldRunAfter(test)
                    }
                }
            }
        }
    }
}

val integrationTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}
val integrationTestRuntimeOnly by configurations.getting {
    extendsFrom(configurations.testRuntimeOnly.get())
}

dependencies {
    implementation(projects.hiveMqttClient)
    implementation(projects.dnsDeviceLocator)

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("io.micrometer:micrometer-registry-otlp")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")
    implementation("io.opentelemetry:opentelemetry-sdk-extension-autoconfigure")
    runtimeOnly(libs.otel.resources)

    implementation(libs.pyroscope)

    implementation(libs.bundles.hivemq.reactive)

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.bundles.influx) {
        exclude("io.micrometer")
    }

    implementation(libs.slack.appender)
    implementation(libs.httpclient.slack.client)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation(libs.wiremock)

    integrationTestImplementation(libs.moquette.broker) {
        exclude(group = "org.slf4j")
        exclude(group = "com.zaxxer")
        exclude(group = "com.h2database")
        exclude(group = "io.dropwizard.metrics")
        exclude(group = "com.librato.metrics")
        exclude(group = "com.bugsnag")
    }
}

application {
    mainClass.set(mainClassKt)
    applicationDefaultJvmArgs =
        listOfNotNull(
            "-XX:+DisableAttachMechanism",
            "-Dcom.sun.management.jmxremote",
            "-Dcom.sun.management.jmxremote.port=9000",
            "-Dcom.sun.management.jmxremote.local.only=false",
            "-Dcom.sun.management.jmxremote.authenticate=false",
            "-Dcom.sun.management.jmxremote.ssl=false",
            "-Dcom.sun.management.jmxremote.rmi.port=9000",
            "-Djava.rmi.server.hostname=127.0.0.1",
        )
}

jib {
    from {
        image = "azul/zulu-openjdk:21-jre-headless"
    }
    container {
        appRoot = "/opt/aqm"
        workingDirectory = "/opt/aqm"
        val javaToolOptions =
            listOfNotNull(
                "-XX:InitialRAMPercentage=50",
                "-XX:MaxRAMPercentage=85",
            ).joinToString(separator = " ")
        environment =
            mapOf(
                "DYSON_CACHE_DIR" to "/opt/aqm/cache",
                "JAVA_TOOL_OPTIONS" to javaToolOptions,
                "SPRING_PROFILES_ACTIVE" to "prod",
            )
        jvmFlags =
            listOfNotNull(
                "-XX:+PrintCommandLineFlags",
                "-XX:+DisableExplicitGC",
            )
        mainClass = mainClassKt
        volumes = listOfNotNull("/opt/aqm/cache")
        creationTime.set("USE_CURRENT_TIMESTAMP")
    }
}
