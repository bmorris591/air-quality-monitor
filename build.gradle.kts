import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.CONFIGURATION_DETEKT
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.getSupportedKotlinVersion
import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile
import org.springframework.boot.gradle.plugin.SpringBootPlugin

plugins {
    java
    application
    `project-report`
    alias(libs.plugins.graalvm) apply false
    alias(libs.plugins.spring.boot) apply false
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.git.props)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)
}

apply(plugin = "io.spring.dependency-management")

group = "uk.co.borismorris.dyson.airqualitymonitor"

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest(layout.buildDirectory.file("detket.config"))
    onlyIfModified(true)
    useETag("all")
}

allprojects {
    apply(plugin = "java")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jetbrains.kotlin.plugin.spring")
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "com.github.ben-manes.versions")
    apply(plugin = "com.gorylenko.gradle-git-properties")
    apply(plugin = "com.diffplug.spotless")
    apply(plugin = "io.gitlab.arturbosch.detekt")

    group = rootProject.group

    java {
        sourceCompatibility = JavaVersion.VERSION_21
        targetCompatibility = JavaVersion.VERSION_21
    }

    tasks.withType<KotlinJvmCompile> {
        compilerOptions {
            jvmTarget.set(JvmTarget.JVM_21)
            freeCompilerArgs.addAll(
                "-Xjsr305=strict",
                "-Xcontext-receivers",
            )
        }
    }

    allOpen {
        annotations("io.micrometer.observation.annotation.Observed")
    }

    kotlin {
        sourceSets.all {
            languageSettings {
                languageVersion = "2.0"
                progressiveMode = true
                optIn("kotlin.RequiresOptIn")
                optIn("kotlin.ExperimentalUnsignedTypes")
                optIn("kotlin.ExperimentalStdlibApi")
                optIn("kotlin.time.ExperimentalTime")
                optIn("kotlin.io.path.ExperimentalPathApi")
            }
        }
    }

    configurations.all {
        exclude(module = "spring-boot-starter-logging")
    }

    configurations.matching { it.name == CONFIGURATION_DETEKT }.all {
        resolutionStrategy.eachDependency {
            if (requested.group == "org.jetbrains.kotlin") {
                useVersion(getSupportedKotlinVersion())
            }
        }
    }

    configure<DependencyManagementExtension> {
        imports {
            mavenBom(SpringBootPlugin.BOM_COORDINATES)
        }
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))

        implementation(rootProject.libs.kotlin.logging)

        implementation("io.micrometer:micrometer-core")
        implementation("io.micrometer:micrometer-tracing")

        testImplementation("org.junit.jupiter:junit-jupiter-api")
        testImplementation("org.junit.jupiter:junit-jupiter-params")
        testImplementation("org.assertj:assertj-core")
        testImplementation(rootProject.libs.awaitility)
        testImplementation("org.mockito:mockito-core")
        testImplementation("org.mockito:mockito-junit-jupiter")
        testImplementation(rootProject.libs.mockito.kotlin)
        testImplementation(kotlin("test-junit5"))
    }

    spotless {
        kotlin {
            ktlint().editorConfigOverride(
                mapOf(
                    "ktlint_standard_no-wildcard-imports" to "disabled",
                ),
            )
        }
        kotlinGradle {
            ktlint()
        }
    }

    detekt {
        buildUponDefaultConfig = true
        allRules = true
        config.setFrom(files(downloadDetektConfig.get().dest))
    }

    tasks["check"].dependsOn("detektMain")

    tasks.withType<Detekt> {
        jvmTarget = "20"
        dependsOn(downloadDetektConfig)
    }

    testing {
        suites {
            withType<JvmTestSuite> {
                useJUnitJupiter()
                targets {
                    all {
                        testTask.configure {
                            testLogging {
                                exceptionFormat = FULL
                                showStandardStreams = true
                                events("skipped", "failed")
                            }
                        }
                    }
                }
            }
        }
    }
}

tasks.htmlDependencyReport {
    projects = project.allprojects
}
