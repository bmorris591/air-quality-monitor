@file:JvmName("DysonDeviceJacksonModule")

package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver
import com.fasterxml.jackson.databind.module.SimpleModule
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice

val heatCapableDeviceModule = SimpleModule("HeatCapableDevice").also {
    val resolver = SimpleAbstractTypeResolver()
    resolver.addMapping(DeviceStatusData::class.java, CurrentHeatCapableDeviceState::class.java)
    resolver.addMapping(DeviceStatusChange::class.java, HeatCapableDeviceStateChange::class.java)
    it.setAbstractTypes(resolver)
}

val nonHeatCapableDeviceModule = SimpleModule("NonHeatCapableDevice").also {
    val resolver = SimpleAbstractTypeResolver()
    resolver.addMapping(DeviceStatusData::class.java, CurrentDeviceState::class.java)
    resolver.addMapping(DeviceStatusChange::class.java, DeviceStateChange::class.java)
    it.setAbstractTypes(resolver)
}

fun registerDeviceStateModule(objectMapper: ObjectMapper, device: LocatedDevice): ObjectMapper = if (device.heatCapable) {
    objectMapper.registerModule(heatCapableDeviceModule)
} else {
    objectMapper.registerModule(nonHeatCapableDeviceModule)
}

@JvmName("registerDeviceStateModuleExt")
fun ObjectMapper.registerDeviceStateModule(device: LocatedDevice): ObjectMapper = registerDeviceStateModule(this, device)
