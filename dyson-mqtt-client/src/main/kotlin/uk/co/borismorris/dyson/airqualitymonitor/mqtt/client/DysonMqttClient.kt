package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.BeanProperty
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import io.github.oshai.kotlinlogging.KotlinLogging
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import java.time.Duration
import java.time.ZonedDateTime
import java.util.*

private val logger = KotlinLogging.logger {}

interface DysonMqttClient : AutoCloseable {
    fun connect(device: LocatedDevice): DysonMqttConnection
}

interface DysonMqttConnection : AutoCloseable {
    val device: LocatedDevice

    fun monitorDevice(): BlockingDysonMqttSubscription
}

interface BlockingDysonMqttSubscription : Iterator<MqttResponseMessage>, AutoCloseable

@JsonDeserialize(`as` = MutableEnvironmentData::class)
interface EnvironmentData {
    val temperature: Double?
    val humidity: Int?
    val pm25: Int?
    val pm10: Int?
    val voc: Int?
    val nox: Int?
    val p25r: Int?
    val p10r: Int?
}

@JsonIgnoreProperties("sltm")
class MutableEnvironmentData : EnvironmentData {
    @JsonProperty("tact")
    @JsonDeserialize(using = DivByTenDeserializer::class)
    override var temperature: Double? = null

    @JsonProperty("hact")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var humidity: Int? = null

    @JsonProperty("pm25")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var pm25: Int? = null

    @JsonProperty("pm10")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var pm10: Int? = null

    @JsonProperty("va10")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var voc: Int? = null

    @JsonProperty("noxl")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var nox: Int? = null

    @JsonProperty("p25r")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var p25r: Int? = null

    @JsonProperty("p10r")
    @JsonDeserialize(using = FailAwareIntDeserializer::class)
    override var p10r: Int? = null

    override fun toString(): String {
        return "EnvironmentData(temperature=$temperature, humidity=$humidity, pm25=$pm25, pm10=$pm10, voc=$voc, nox=$nox, p25r=$p25r, p10r=$p10r)"
    }
}

interface DeviceStatusData {
    val fanStatus: ToggleState
    val fanDirection: FanDirection
    val autoMode: ToggleState
    val oscillationState: OscillationState
    val nightMode: ToggleState
    val continuousMonitoring: ToggleState
    val fanState: FanState
    val nightModeSpeed: Int
    val fanSpeed: FanSpeed
    val carbonFilterState: Int
    val hepaFilterState: Int
    val sleepTimer: SleepTimer
    val oscillationAngleLow: Int
    val oscillationAngleHigh: Int
}

interface DeviceStatusChange : DeviceStatusData {
    val fanStatusChange: PropertyChange<ToggleState>
    val fanDirectionChange: PropertyChange<FanDirection>
    val autoModeChange: PropertyChange<ToggleState>
    val oscillationStateChange: PropertyChange<OscillationState>
    val nightModeChange: PropertyChange<ToggleState>
    val continuousMonitoringChange: PropertyChange<ToggleState>
    val fanStateChange: PropertyChange<FanState>
    val nightModeSpeedChange: PropertyChange<Int>
    val fanSpeedChange: PropertyChange<FanSpeed>
    val carbonFilterStateChange: PropertyChange<Int>
    val hepaFilterStateChange: PropertyChange<Int>
    val sleepTimerChange: PropertyChange<SleepTimer>
    val oscillationAngleLowChange: PropertyChange<Int>
    val oscillationAngleHighChange: PropertyChange<Int>

    override val fanStatus: ToggleState
        get() = fanStatusChange.newValue
    override val fanDirection: FanDirection
        get() = fanDirectionChange.newValue
    override val autoMode: ToggleState
        get() = autoModeChange.newValue
    override val oscillationState: OscillationState
        get() = oscillationStateChange.newValue
    override val nightMode: ToggleState
        get() = nightModeChange.newValue
    override val continuousMonitoring: ToggleState
        get() = continuousMonitoringChange.newValue
    override val fanState: FanState
        get() = fanStateChange.newValue
    override val nightModeSpeed: Int
        get() = nightModeSpeedChange.newValue
    override val fanSpeed: FanSpeed
        get() = fanSpeedChange.newValue
    override val carbonFilterState: Int
        get() = carbonFilterStateChange.newValue
    override val hepaFilterState: Int
        get() = hepaFilterStateChange.newValue
    override val sleepTimer: SleepTimer
        get() = sleepTimerChange.newValue
    override val oscillationAngleLow: Int
        get() = oscillationAngleLowChange.newValue
    override val oscillationAngleHigh: Int
        get() = oscillationAngleHighChange.newValue
}

interface HeatCapableDeviceStatusData : DeviceStatusData {
    val tiltState: TiltState
    val heatTarget: Int
    val heatMode: HeatMode
    val heatState: HeatMode
}

interface HeatCapableDeviceStatusChange : DeviceStatusChange, HeatCapableDeviceStatusData {
    val tiltStateChange: PropertyChange<TiltState>
    val heatTargetChange: PropertyChange<Int>
    val heatModeChange: PropertyChange<HeatMode>
    val heatStateChange: PropertyChange<HeatMode>

    override val tiltState: TiltState
        get() = tiltStateChange.newValue
    override val heatTarget: Int
        get() = heatTargetChange.newValue
    override val heatMode: HeatMode
        get() = heatModeChange.newValue
    override val heatState: HeatMode
        get() = heatStateChange.newValue
}

enum class ToggleState {
    ON,
    OFF,
}

enum class HeatMode {
    HEAT,
    OFF,
}

enum class TiltState {
    OK,
    TILT,
}

enum class OscillationState {
    ON,
    IDLE,
    OFF,
}

enum class FanDirection {
    FRONT,
    BACK, ;

    companion object {
        @JsonCreator
        @JvmStatic
        fun forValue(value: String) = if (value == "ON") FRONT else BACK
    }
}

enum class FanState {
    OFF,
    FAN,
    AUTO,
}

@JvmRecord
data class FanSpeed(val speed: Int, val auto: Boolean) {
    companion object {
        @JsonCreator
        @JvmStatic
        fun forValue(value: String) = if (value == "AUTO") {
            FanSpeed(0, true)
        } else {
            FanSpeed(value.toInt(), false)
        }
    }
}

@JvmRecord
data class SleepTimer(val remaining: Duration, val enabled: Boolean) {
    companion object {
        @JsonCreator
        @JvmStatic
        fun forValue(value: String) = if (value == "OFF") {
            SleepTimer(Duration.ZERO, false)
        } else {
            SleepTimer(Duration.ofMinutes(value.toLong()), true)
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
open class CurrentDeviceState : DeviceStatusData {
    @JsonProperty("fpwr")
    override var fanStatus: ToggleState = ToggleState.OFF

    @JsonProperty("fdir")
    override var fanDirection: FanDirection = FanDirection.FRONT

    @JsonProperty("auto")
    override var autoMode: ToggleState = ToggleState.OFF

    @JsonProperty("oscs")
    override var oscillationState: OscillationState = OscillationState.OFF

    @JsonProperty("nmod")
    override var nightMode: ToggleState = ToggleState.OFF

    @JsonProperty("rhtm")
    override var continuousMonitoring: ToggleState = ToggleState.OFF

    @JsonProperty("fnst")
    override var fanState: FanState = FanState.OFF

    @JsonProperty("nmdv")
    override var nightModeSpeed: Int = -1

    @JsonProperty("fnsp")
    override var fanSpeed: FanSpeed = FanSpeed(0, false)

    @JsonProperty("cflr")
    override var carbonFilterState: Int = -1

    @JsonProperty("hflr")
    override var hepaFilterState: Int = -1

    @JsonProperty("sltm")
    override var sleepTimer: SleepTimer = SleepTimer(Duration.ZERO, false)

    @JsonProperty("osal")
    override var oscillationAngleLow: Int = -1

    @JsonProperty("osau")
    override var oscillationAngleHigh: Int = -1

    override fun toString(): String {
        return "CurrentDeviceState(fanStatus=$fanStatus, fanDirection=$fanDirection, autoMode=$autoMode, oscillationState=$oscillationState, nightMode=$nightMode, continuousMonitoring=$continuousMonitoring, fanState=$fanState, nightModeSpeed=$nightModeSpeed, fanSpeed=$fanSpeed, carbonFilterState=$carbonFilterState, hepaFilterState=$hepaFilterState, sleepTimer=$sleepTimer, oscillationAngleLow=$oscillationAngleLow, oscillationAngleHigh=$oscillationAngleHigh)"
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class CurrentHeatCapableDeviceState : CurrentDeviceState(), HeatCapableDeviceStatusData {
    @JsonProperty("tilt")
    override var tiltState: TiltState = TiltState.OK

    @JsonProperty("hmax")
    override var heatTarget: Int = 0

    @JsonProperty("hmod")
    override var heatMode: HeatMode = HeatMode.OFF

    @JsonProperty("hsta")
    override var heatState: HeatMode = HeatMode.OFF

    override fun toString(): String {
        return "CurrentHeatCapableDeviceState(tiltState=$tiltState, heatTarget=$heatTarget, heatMode=$heatMode, heatState=$heatState) ${super.toString()}"
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
open class DeviceStateChange : DeviceStatusChange {
    @JsonProperty("fpwr")
    override var fanStatusChange: PropertyChange<ToggleState> = PropertyChange.unchanged(ToggleState.OFF)

    @JsonProperty("fdir")
    override var fanDirectionChange: PropertyChange<FanDirection> = PropertyChange.unchanged(FanDirection.FRONT)

    @JsonProperty("auto")
    override var autoModeChange: PropertyChange<ToggleState> = PropertyChange.unchanged(ToggleState.OFF)

    @JsonProperty("oscs")
    override var oscillationStateChange: PropertyChange<OscillationState> =
        PropertyChange.unchanged(OscillationState.OFF)

    @JsonProperty("nmod")
    override var nightModeChange: PropertyChange<ToggleState> = PropertyChange.unchanged(ToggleState.OFF)

    @JsonProperty("rhtm")
    override var continuousMonitoringChange: PropertyChange<ToggleState> = PropertyChange.unchanged(ToggleState.OFF)

    @JsonProperty("fnst")
    override var fanStateChange: PropertyChange<FanState> = PropertyChange.unchanged(FanState.OFF)

    @JsonProperty("nmdv")
    override var nightModeSpeedChange: PropertyChange<Int> = PropertyChange.unchanged(-1)

    @JsonProperty("fnsp")
    override var fanSpeedChange: PropertyChange<FanSpeed> = PropertyChange.unchanged(FanSpeed(0, false))

    @JsonProperty("cflr")
    override var carbonFilterStateChange: PropertyChange<Int> = PropertyChange.unchanged(-1)

    @JsonProperty("hflr")
    override var hepaFilterStateChange: PropertyChange<Int> = PropertyChange.unchanged(-1)

    @JsonProperty("sltm")
    override var sleepTimerChange: PropertyChange<SleepTimer> =
        PropertyChange.unchanged(SleepTimer(Duration.ZERO, false))

    @JsonProperty("osal")
    override var oscillationAngleLowChange: PropertyChange<Int> = PropertyChange.unchanged(-1)

    @JsonProperty("osau")
    override var oscillationAngleHighChange: PropertyChange<Int> = PropertyChange.unchanged(-1)

    override fun toString(): String {
        return "DeviceStateChange(fanStatusChange=$fanStatusChange, fanDirectionChange=$fanDirectionChange, autoModeChange=$autoModeChange, oscillationStateChange=$oscillationStateChange, nightModeChange=$nightModeChange, continuousMonitoringChange=$continuousMonitoringChange, fanStateChange=$fanStateChange, nightModeSpeedChange=$nightModeSpeedChange, fanSpeedChange=$fanSpeedChange, carbonFilterStateChange=$carbonFilterStateChange, hepaFilterStateChange=$hepaFilterStateChange, sleepTimerChange=$sleepTimerChange, oscillationAngleLowChange=$oscillationAngleLowChange, oscillationAngleHighChange=$oscillationAngleHighChange)"
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class HeatCapableDeviceStateChange : DeviceStateChange(), HeatCapableDeviceStatusChange {
    @JsonProperty("tilt")
    override var tiltStateChange: PropertyChange<TiltState> = PropertyChange.unchanged(TiltState.OK)

    @JsonProperty("hmax")
    override var heatTargetChange: PropertyChange<Int> = PropertyChange.unchanged(0)

    @JsonProperty("hmod")
    override var heatModeChange: PropertyChange<HeatMode> = PropertyChange.unchanged(HeatMode.OFF)

    @JsonProperty("hsta")
    override var heatStateChange: PropertyChange<HeatMode> = PropertyChange.unchanged(HeatMode.OFF)

    override fun toString(): String {
        return "HeatCapableDeviceStateChange(tiltStateChange=$tiltStateChange, heatTargetChange=$heatTargetChange, heatModeChange=$heatModeChange, heatStateChange=$heatStateChange) ${super.toString()}"
    }
}

@JsonDeserialize(using = PropertyChangeDeserializer::class)
@JsonSerialize(using = PropertyChangeSerializer::class)
interface PropertyChange<T> {
    companion object {
        @JvmStatic
        fun <T> unchanged(value: T): PropertyChange<T> = UnchangedProperty(value)

        @JvmStatic
        fun <T> change(old: T, new: T): PropertyChange<T> = if (old == new) {
            UnchangedProperty(old)
        } else {
            ChangedProperty(old, new)
        }
    }

    val oldValue: T
    val newValue: T

    fun changed(): Boolean

    fun ifChanged(valueConsumer: PropertyChange<T>.() -> Unit)
}

@JvmRecord
data class ChangedProperty<T>(override val oldValue: T, override val newValue: T) : PropertyChange<T> {
    override fun changed() = true

    override fun ifChanged(valueConsumer: PropertyChange<T>.() -> Unit) {
        valueConsumer(this)
    }
}

@JvmRecord
data class UnchangedProperty<T>(private val value: T) : PropertyChange<T> {

    override val newValue: T
        get() = value

    override val oldValue: T
        get() = value

    override fun changed() = false

    override fun ifChanged(valueConsumer: PropertyChange<T>.() -> Unit) {}
}

class PropertyChangeDeserializer(private val type: JavaType?) :
    JsonDeserializer<PropertyChange<Any>>(),
    ContextualDeserializer {

    constructor() : this(null)

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): PropertyChange<Any> {
        Objects.requireNonNull(type)
        val backingCollectionType = ctxt.typeFactory.constructCollectionLikeType(List::class.java, type)
        val values = ctxt.readValue<List<Any>>(p, backingCollectionType)
        return PropertyChange.change(values[0], values[1])
    }

    override fun createContextual(ctxt: DeserializationContext, property: BeanProperty): JsonDeserializer<*> {
        val wrapperType = property.type
        val valueType = wrapperType.containedType(0)
        return PropertyChangeDeserializer(valueType)
    }
}

class PropertyChangeSerializer : JsonSerializer<PropertyChange<Any>>() {
    override fun serialize(value: PropertyChange<Any>?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        checkNotNull(value)
        checkNotNull(gen)
        checkNotNull(serializers)

        gen.writeStartArray()
        gen.writeObject(value.oldValue)
        gen.writeObject(value.newValue)
        gen.writeEndArray()
    }
}

private const val DEVICE_STATE_CHANGE_STRING = "STATE-CHANGE"
private const val DEVICE_CURRENT_STATE_STRING = "CURRENT-STATE"
private const val ENVIRONMENT_DATA_STRING = "ENVIRONMENTAL-CURRENT-SENSOR-DATA"

enum class MessageType(val type: String) {
    DEVICE_STATE_CHANGE(DEVICE_STATE_CHANGE_STRING),
    DEVICE_CURRENT_STATE(DEVICE_CURRENT_STATE_STRING),
    ENVIRONMENT_DATA(ENVIRONMENT_DATA_STRING),
    REQUEST_ENVIRONMENT_DATA("REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA"), ;

    companion object {
        private val values = values()

        @JsonCreator
        @JvmStatic
        fun forValue(value: String) = values.first { it.type == value }
    }

    @JsonValue
    fun toValue() = type
}

class DivByTenDeserializer : StdDeserializer<Double>(Double::class.java) {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?) =
        _parseIntPrimitive(p, ctxt).let { if (it == 0) 0.0 else it.toDouble() / 10 }
}

class FailAwareIntDeserializer : StdDeserializer<Int>(Int::class.java) {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Int? = try {
        _parseIntPrimitive(p, ctxt)
    } catch (mme: MismatchedInputException) {
        logger.debug(mme) { "Failed to parse value. Assuming FAIL" }
        null
    }
}

@JvmRecord
data class MqttRequestMessage(
    @JsonProperty("msg")
    val type: MessageType,
    @JsonProperty("time")
    @JsonFormat(locale = "en_GB", timezone = "Zulu", pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
    val time: ZonedDateTime,
)

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "msg",
    visible = true,
)
@JsonSubTypes(
    Type(value = MqttDeviceStatusChangeResponse::class, name = DEVICE_STATE_CHANGE_STRING),
    Type(value = MqttDeviceStatusResponse::class, name = DEVICE_CURRENT_STATE_STRING),
    Type(value = MqttEnvironmentDataResponse::class, name = ENVIRONMENT_DATA_STRING),
)
sealed class MqttResponseMessage {
    @JacksonInject
    @JsonIgnore
    lateinit var device: LocatedDevice

    @JsonProperty("msg")
    lateinit var type: MessageType

    @JsonProperty("time")
    lateinit var time: ZonedDateTime

    override fun toString(): String {
        return "MqttResponseMessage(type=$type, time=$time)"
    }
}

data class MqttEnvironmentDataResponse(val data: EnvironmentData) : MqttResponseMessage() {
    override fun toString() = "MqttEnvironmentDataResponse(data=$data) ${super.toString()}"
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class MqttDeviceStatusResponse(
    @JsonProperty("product-state")
    val data: DeviceStatusData,
) : MqttResponseMessage() {
    override fun toString() = "MqttDeviceStatusResponse(data=$data) ${super.toString()}"
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class MqttDeviceStatusChangeResponse(
    @JsonProperty("product-state")
    val data: DeviceStatusChange,
) : MqttResponseMessage() {
    override fun toString() = "MqttDeviceStatusChangeResponse(data=$data)"
}

enum class Topic(private val suffix: String) {
    COMMAND("command"),
    STATUS("status/current"), ;

    fun topicForDevice(device: LocatedDevice) = "${device.type}/${device.serial}/$suffix"
}

fun LocatedDevice.commandTopic() = Topic.COMMAND.topicForDevice(this)
fun LocatedDevice.statusTopic() = Topic.STATUS.topicForDevice(this)
